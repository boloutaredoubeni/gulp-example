// This is an example gulpfile
'use strict';

// TODO: move all tasks to seperate gulp files
// TODO: import config from package.json
// TODO: test these tasks

const gulp = require('gulp');
const concat = require('gulp-concat');
const myth = require('gulp-myth');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const jshint = require('gulp-jshint');
const traceur = require('gulp-traceur');
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin');
const connect = require('connect');
const serve = require('serve-static');
const browsersync = require('browser-sync');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const plumber = require('gulp-plumber');
const beeper = require('beeper');
const del = require('del');
const config = require('./package.json').project;

// Error Helper
function onError(err) {
  beeper();
  console.log(err);
}


// A task to concat css files and move to a build directory
gulp.task('styles:css', () => {
  return gulp.src(config.src.styles)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(concat('main.min.css'))
    .pipe(myth())
    .pipe(gulp.dest(config.dest));
});

// A task to build sass files and move to a build directory
gulp.task('styles:sass', () => {
  return gulp.src('app/scss/**/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest('dist'));
});

gulp.task('scripts:js', () => {
  return gulp.src(config.src.scripts)
    .pipe(sourcemaps.init())
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(traceur())
    .pipe(concat('all.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.dest));
});

gulp.task('images', () => {
  return gulp.src('app/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
});

gulp.task('watch', () => {
  gulp.watch('app/scss/*.{scss,sass}', gulp.series(
    'clean:css', 'styles:sass', browsersync.reload
  ))
  gulp.watch('app/js/*.js', gulp.series(
    'clean:js', 'scripts:js', browsersync.reload
  ))
  // TODO: add clean task
  gulp.watch('app/img/*', gulp.series('images', browsersync.reload));
});

gulp.task('serve:static', () => {
  return connect().use(serve(__dirname))
    .listen(8080)
    .on('listening', () => {
      console.log('Server Running at http://localhost:8080');
    });
});

gulp.task('browsersync', (callback) => {
  return browsersync({
    server: {
        baseDir: './'
    }
  }, callback);
});

gulp.task('browserify', function() {
  return browserify('./app/js/app.js')
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('dist'));
});

gulp.task('clean:css', (callback) => {
  del([config.dest + '/**/*.css'], callback);
});

gulp.task('clean:js', (callback) => {
  del([config.dest + '/**/*.js'], callback);
});

gulp.task('clean', (callback) => {
  gulp.series('clean:css', 'clean:js');
});

// TODO: configure jade

gulp.task('default', gulp.parallel(
    'styles:sass', 
    'scripts:js', 
    'images', 
    'watch', 
    // 'serve:static',
    'browsersync'
));

